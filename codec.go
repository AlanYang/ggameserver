package game

var (
	DefaultPacket Packet
)

type (
	Encoder interface {
		WriteHeader([]byte, []byte)
		WriteBody(body []byte)
		Bytes() []byte
	}

	Decoder interface {
		ReadHeader() ([]byte, []byte)
		ReadBody() []byte
	}

	Packet interface {
		Encoder
		Decoder
		Flip()
	}

	BinaryPacket struct {
		data []byte
	}
)

func (bp *BinaryPacket) WriteHeader(from, to []byte) {
	if len(from) == 0 {
		from = make([]byte, 16)
	}

	if len(to) == 0 {
		to = make([]byte, 16)
	}
	bp.data = append(bp.data, from...)
	bp.data = append(bp.data, to...)
}

func (bp *BinaryPacket) WriteBody(body []byte) {
	bp.data = append(bp.data, body...)
}

func (bp *BinaryPacket) Bytes() []byte {
	return bp.data
}

func (bp *BinaryPacket) ReadHeader() (from, to []byte) {
	from = bp.data[:16]
	to = bp.data[16:32]
	return
}

func (bp *BinaryPacket) ReadBody() []byte {
	return bp.data[32:]
}

func (bp *BinaryPacket) Flip() {
	bp.data = []byte{}
}

func init() {
	DefaultPacket = &BinaryPacket{
		data: []byte{},
	}
}
