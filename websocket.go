package game

import (
	"github.com/gorilla/websocket"
	"net/http"
)

const (
	WriteBufferSize = 2048
	ReadBufferSize  = 2048
)

var (
	upgrader = websocket.Upgrader{
		ReadBufferSize:  ReadBufferSize,
		WriteBufferSize: WriteBufferSize,
	}
)

type (
	WebSocketTransport struct {
		Conn  *websocket.Conn
		Event EventLoop
		ID    ID
	}
)

func NewWebSocketTransport(resp http.ResponseWriter, req *http.Request, ev EventLoop) (*WebSocketTransport, error) {
	ws := new(WebSocketTransport)

	conn, err := upgrader.Upgrade(resp, req, nil)

	if err != nil {
		return nil, err
	}

	ws.Conn = conn

	if ev != nil {
		ws.Activate(ev)
	}

	return ws, err
}

func (w *WebSocketTransport) Activate(ev EventLoop) {
	w.Event = ev
	w.ID, _ = ev.Join(NewID(), w)
}

func (w *WebSocketTransport) Write(m *Message) error {
	return w.Conn.WriteMessage(1, m.Payload)
}

func (w *WebSocketTransport) Close() error {
	return w.Conn.Close()
}

func (w *WebSocketTransport) Activated() bool {
	return w.Event != nil
}
