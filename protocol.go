package game

import (
	"log"
)

const (
	COMMON_HEART = iota + 1
	COMMON_JOIN
	COMMON_LEVEL
)

const ()

type (
	Protocol interface {
		Process([]byte)
	}

	Game struct {
		Name  string
		world *Logic
		lobby *Logic
		room  map[string]*Logic
	}

	Logic struct {
		Name  string
		event EventLoop
		cap   int
	}

	GameOption struct {
		WorldCap int
		LobbyCap int
		RoomCap  int
	}
)

func NewDefaultGameOption() *GameOption {
	return &GameOption{
		WorldCap: 65536,
		LobbyCap: 40960,
		RoomCap:  4096,
	}
}

func NewGame(name string, opt *GameOption) *Game {
	if opt == nil {
		opt = NewDefaultGameOption()
	}

	world, err := NewEvent("world", nil)
	if err != nil {
		log.Fatal(err)
	}

	lobby, err := NewEvent("lobby", nil)
	if err != nil {
		log.Fatal(err)
	}
	return &Game{
		Name: name,

		world: &Logic{
			Name:  "world",
			event: world,
			cap:   opt.WorldCap,
		},

		lobby: &Logic{
			Name:  "lobby",
			event: lobby,
			cap:   opt.LobbyCap,
		},

		room: make(map[string]*Logic),
	}
}

func (g *Game) Process(data []byte) {
}
