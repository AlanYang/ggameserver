package game

import (
	"html/template"
	"net"
	"net/http"
)

type (
	Option struct {
		Port int
	}
)

func WebSocket(resp http.ResponseWriter, req *http.Request) {
	ip, port, _ := net.SplitHostPort(req.RemoteAddr)
	Logger.Infof("%s:%s connected", ip, port)
	ws, err := NewWebSocketTransport(resp, req, DefaultEvent())
	if err != nil {
		Logger.Error(err)
		return
	}
	defer ws.Conn.Close()

	hub := ws.Event
	for {
		m, b, err := ws.Conn.ReadMessage()
		if err != nil {
			Logger.Error(err)
			Logger.Infof("%s is disconnected.", ws.ID.String())
			return
		}
		Logger.Infof("Message type: %d, Payload: %s", m, string(b))
		hub.Dispatch(ws.ID, b, func(clients map[ID]*Client) (ids []ID) {
			ids = []ID{}
			for k, _ := range clients {
				if k != ws.ID {
					ids = append(ids, k)
				}
			}
			if len(ids) == 0 {
				ids = append(ids, EmptyID())
			}
			return
		})
	}
}

func Start(port string) {
	Logger.Infof("Start Game Server on :8888")
	if port == "" {
		port = ":8888"
	}
	http.HandleFunc("/event", WebSocket)
	http.HandleFunc("/", func(resp http.ResponseWriter, req *http.Request) {
		homeTemplate.Execute(resp, "ws://"+req.Host+"/event")
	})
	Logger.Fatal(http.ListenAndServe(port, nil))
}

var homeTemplate = template.Must(template.New("").Parse(`
<!DOCTYPE html>
<head>
<meta charset="utf-8">
<script>
window.addEventListener("load", function(evt) {
    var output = document.getElementById("output");
    var input = document.getElementById("input");
    var ws;
    var print = function(message) {
        var d = document.createElement("div");
        d.innerHTML = message;
        output.appendChild(d);
    };
    document.getElementById("open").onclick = function(evt) {
        if (ws) {
            return false;
        }
        ws = new WebSocket("{{.}}");
        ws.onopen = function(evt) {
            print("OPEN");
        }
        ws.onclose = function(evt) {
            print("CLOSE");
            ws = null;
        }
        ws.onmessage = function(evt) {
            print("Message: " + evt.data);
        }
        ws.onerror = function(evt) {
            print("ERROR: " + evt.data);
        }
        return false;
    };
    document.getElementById("send").onclick = function(evt) {
        if (!ws) {
            return false;
        }
        print("SEND: " + input.value);
        ws.send(input.value);
        return false;
    };
    document.getElementById("close").onclick = function(evt) {
        if (!ws) {
            return false;
        }
        ws.close();
        return false;
    };
});
</script>
</head>
<body>
<table>
<tr><td valign="top" width="50%">
<form>
<button id="open">Open</button>
<button id="close">Close</button>
<p><input id="input" type="text">
<button id="send">Send</button>
</form>
</td><td valign="top" width="50%">
<div id="output"></div>
</td></tr></table>
</body>
</html>
`))
