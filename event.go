package game

import (
	"errors"
	"github.com/satori/go.uuid"
	"github.com/sirupsen/logrus"
	"os"
	"sync"
	"time"
)

var (
	defaultEvent *Event
	Logger       *logrus.Entry
	pool         *eventPool
)

type (
	eventPool struct {
		locker *sync.RWMutex
		events map[string]EventLoop
	}

	EventLoop interface {
		//Join to event space
		Join(ID, WriterCloser) (ID, error)

		//leave
		Leave(ID)

		//broadcast to targets
		Broadcast(ID, []ID, []byte)
		Dispatch(ID, []byte, Selector)
		Request(ID, ID, []byte)
		String() string

		Start()
		Stop()
		Close()
	}

	Message struct {
		To      []ID
		From    ID
		Payload []byte
	}

	Client struct {
		Transport WriterCloser
		errCount  int
		Id        ID
	}

	EventOption struct {
		Encoder   Packet
		ErrCount  int
		AutoRun   bool
		AddToPool bool
		Breath    time.Duration
	}

	Event struct {
		Name      string
		Clients   map[ID]*Client
		join      chan *Client
		leave     chan ID
		broadcast chan *Message
		request   chan *Message
		stop      chan struct{}
		Option    *EventOption
	}

	ID uuid.UUID

	Id = ID

	WriterCloser interface {
		Write(*Message) error
		Close() error
	}

	Selector func(map[ID]*Client) []ID
)

func NewID() ID {
	return ID(uuid.NewV4())
}

func fromBytes(b []byte) ID {
	id := NewID()
	copy(id[:], b[:])
	return id
}

func (id *ID) Validate() bool {
	if len(id) != 16 {
		return false
	}
	for _, b := range id {
		if b != byte(0) {
			return true
		}
	}
	return false
}

func (id ID) String() string {
	return uuid.UUID(id).String()
}

func (id ID) Bytes() []byte {
	return id[:]
}

func newDefaultEventOption() *EventOption {
	return &EventOption{
		Encoder:   DefaultPacket,
		ErrCount:  3,
		AutoRun:   false,
		AddToPool: true,
		Breath:    time.Millisecond * 100,
	}
}

func NewEvent(name string, opt *EventOption) (*Event, error) {
	if name == "" {
		return nil, errors.New("Event name is required.")
	}

	if opt == nil {
		opt = newDefaultEventOption()
	}
	event := &Event{
		Name:      name,
		Clients:   make(map[ID]*Client),
		join:      make(chan *Client),
		leave:     make(chan ID),
		broadcast: make(chan *Message),
		request:   make(chan *Message),
		stop:      make(chan struct{}),
		Option:    opt,
	}

	if opt.AutoRun {
		go event.Start()
	}

	if opt.AddToPool {
		pool.Add(event.Name, event)
	}

	return event, nil
}

func (ev *Event) String() string {
	return ev.Name
}

func (ev *Event) Start() {
	defer func() {
		if p := recover(); p != nil {
			Logger.Error(p)
		}
	}()

	Logger.Infof("Start event hub [%s]", ev.String())
	for {
		select {
		case cli := <-ev.join:
			ev.Clients[cli.Id] = cli

		case id := <-ev.leave:
			if _, ok := ev.Clients[id]; ok {
				// t.Transport.Close()
				delete(ev.Clients, id)
				Logger.Infof("%s leaved", id.String())
			}

		case m := <-ev.broadcast:
			if len(m.Payload) != 0 {
				if m.To == nil || len(m.To) == 0 {
					for id, t := range ev.Clients {
						err := t.Transport.Write(m)

						if err != nil {
							ev.transportError(id, m.Payload, err)
						}
					}
				} else {
					for _, id := range m.To {
						if t, ok := ev.Clients[id]; ok {
							err := t.Transport.Write(m)
							ev.Option.Encoder.Flip()

							if err != nil {
								ev.transportError(id, m.Payload, err)
							}
						}
					}
				}
			}

		case m := <-ev.request:
			if m.To == nil || len(m.To) == 0 {
				continue
			}
			if t, ok := ev.Clients[m.To[0]]; ok {
				err := t.Transport.Write(m)

				if err != nil {
					ev.transportError(m.To[0], m.Payload, err)
				}
			}

		case <-ev.stop:
			Logger.Infof("Stoped event [%s]...", ev.String())
			goto STOP

		case <-time.After(ev.Option.Breath):

		}
	}

STOP:
	Logger.Info("Stoped event hub")
}

func (ev *Event) Stop() {
	go func() {
		Logger.Infof("Stoping event hub [%s]...", ev.String())
		time.After(time.Microsecond * 1)
		ev.stop <- struct{}{}
		pool.Remove(ev.Name)
		ev.Close()
	}()
}

func (ev *Event) Close() {
	close(ev.join)
	close(ev.leave)
	close(ev.broadcast)
	close(ev.request)
}

func (ev *Event) transportError(id ID, data []byte, err error) {
	c, _ := ev.Clients[id]
	c.errCount++
	Logger.Errorf("Write %x to transport %s failure, reason: %s", data, id, err.Error())
	if c.errCount > 3 {
		ev.leave <- id
		Logger.Errorf("errors %d, closing the transport[%s]", 3, id)
	}
}

func (ev *Event) Join(id ID, t WriterCloser) (ID, error) {
	if !id.Validate() {
		return id, errors.New("Invalidate id.")
	}

	ev.join <- &Client{
		Transport: t,
		Id:        id,
		errCount:  0,
	}
	Logger.Infof("%s join to [%s].", id.String(), ev.Name)
	return id, nil
}

func (ev *Event) Leave(id ID) {
	ev.leave <- id
}

func (ev *Event) Broadcast(id ID, to []ID, d []byte) {
	// Logger.Infof("", to)
	ev.broadcast <- &Message{
		To:      to,
		From:    id,
		Payload: d,
	}
}

func (ev *Event) Dispatch(id ID, d []byte, selector Selector) {
	ev.Broadcast(id, selector(ev.Clients), d)
}

func (ev *Event) Request(from, to ID, d []byte) {
	ev.request <- &Message{
		To:      []ID{to},
		From:    from,
		Payload: d,
	}
}

func (ev *Event) Has(id ID) bool {
	_, ok := ev.Clients[id]
	return ok
}

func (p *eventPool) Add(name string, ev EventLoop) {
	p.locker.Lock()
	defer p.locker.Unlock()

	if name == "" {
		name = ev.String()
	}
	p.events[name] = ev
}

func (p *eventPool) Remove(name string) {
	p.locker.Lock()
	defer p.locker.Unlock()

	delete(p.events, name)
}

func (p *eventPool) Get(name string) EventLoop {
	p.locker.RLock()
	defer p.locker.RUnlock()

	ev, _ := p.events[name]
	return ev
}

func DefaultEvent() *Event {
	if defaultEvent == nil {
		defaultEvent, _ = NewEvent("default", nil)
	}
	go defaultEvent.Start()
	return defaultEvent
}

func GetEvent(name string) EventLoop {
	return pool.Get(name)
}

func EmptyID() ID {
	return fromBytes(make([]byte, 16))
}

func StopAll() chan struct{} {
	ch := make(chan struct{})
	Logger.Info("Stoping all event...")
	go func() {
		pool.locker.RLock()
		defer pool.locker.RUnlock()
		for _, ev := range pool.events {
			ev.Stop()
		}
		time.Sleep(time.Second)
		ch <- struct{}{}
		Logger.Info("Stoped all event.")
	}()

	return ch
}

func init() {
	logrus.SetOutput(os.Stdout)
	logrus.SetFormatter(new(logrus.TextFormatter))
	Logger = logrus.WithFields(logrus.Fields{})

	pool = &eventPool{
		locker: new(sync.RWMutex),
		events: make(map[string]EventLoop),
	}
}
