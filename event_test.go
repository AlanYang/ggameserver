package game

import (
	log "github.com/sirupsen/logrus"
	"testing"
	// "time"
)

type StdOutTransport struct {
}

func (s *StdOutTransport) Write(m *Message) error {
	log.Infof("from %s recvied data [%s]", m.From.String(), string(m.Payload))
	return nil
}

func (s *StdOutTransport) Close() error {
	return nil
}

func Test_HubJoin(t *testing.T) {
	hub := DefaultEvent()
	id, _ := hub.Join(NewID(), new(StdOutTransport))
	hub.Broadcast(id, nil, []byte("I'm stdout."))
	hub.Broadcast(id, nil, []byte("I will leave."))
	hub.Request(id, id, []byte("Last request."))
	hub.Leave(id)
	// hub.Stop()
	// time.Sleep(time.Second * 3)
}

func Test_EventPool(t *testing.T) {
	NewEvent("lobby", nil)
	NewEvent("room", nil)

	// defer lobby.Stop()
	// defer room.Stop()

	// defer time.Sleep(time.Second * 10)

	log.Info(GetEvent("lobby").String())
	log.Info(GetEvent("room").String())

	// time.Sleep(time.Microsecond * 500)
}

func Test_StopAllEvent(t *testing.T) {
	<-StopAll()
}
